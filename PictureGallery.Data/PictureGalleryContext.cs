﻿using PictureGallery.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureGallery.Data
{
    public class PictureGalleryContext :DbContext
    {
        public PictureGalleryContext()
            : base("name=PictureGalleryContext")
        {
        }

        public DbSet<FotoInfo> FotoInfo { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }
    }
}
