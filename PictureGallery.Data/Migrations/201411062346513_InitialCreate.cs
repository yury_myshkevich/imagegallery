namespace PictureGallery.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FotoInfoes",
                c => new
                    {
                        FotoInfoId = c.Int(nullable: false, identity: true),
                        Foto = c.Binary(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.FotoInfoId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FotoInfoes");
        }
    }
}
