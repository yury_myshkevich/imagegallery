﻿using PictureGallery.Data.Infrastructure;
using PictureGallery.Data.Interface;
using PictureGallery.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureGallery.Data.Repository
{
    public class FotoInfoRepository : RepositoryBase<FotoInfo>, IFotoInfoRepository
    {
        private PictureGalleryContext _context;
        public FotoInfoRepository(IDatabaseFactory databaseFactory)
            :base(databaseFactory)
        {
            _context = databaseFactory.Get();
        }

        public override void  Update(FotoInfo entity)
        {
            _context.FotoInfo.FirstOrDefault(x => x.FotoInfoId == entity.FotoInfoId).Comment = entity.Comment;
            _context.SaveChanges();
        }
    }
}
