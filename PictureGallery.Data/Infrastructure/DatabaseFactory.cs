﻿using PictureGallery.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PictureGallery.Data.Infrastructure
{
    public class DatabaseFactory: Disposable, IDatabaseFactory
    {
        private PictureGalleryContext _context;

        public PictureGalleryContext Get()
        {
            return _context ?? (_context = new PictureGalleryContext());
        }

        protected override void DisposeCore()
        {
            if (_context != null)
                _context.Dispose();
        }
    }
}
