﻿using PictureGallery.Data.Interface;
using PictureGallery.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureGallery.Data.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
        private PictureGalleryContext _context;
        private  IDbSet<T> _dbset;
        protected IDatabaseFactory DatabaseFactory { get; private set; }
        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            _dbset = Context.Set<T>();
        }

        protected PictureGalleryContext Context
        {
            get { return _context ?? (_context = DatabaseFactory.Get()); }
        }
        public virtual void Add(T entity)
        {
            _dbset.Add(entity);
        }
        public virtual void Update(T entity)
        {
            _dbset.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(T entity)
        {
            _dbset.Remove(entity);
        }

        public virtual T GetById(int id)
        {
            return _dbset.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbset.ToList();
        }
    }
}