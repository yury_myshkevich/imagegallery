﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PictureGallery.Data;
using PictureGallery.Model.Models;

namespace PictureGallery.Data.Interface
{
    public interface IFotoInfoRepository: IRepository<FotoInfo>
    {
    }
}
