﻿using PictureGallery.Model.Models;
using PictureGallery.Service.Interface;
using PictureGallery.WebUI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PictureGallery.WebUI.Controllers
{
    public class FotoInfoController : ApiController
    {
        private IFotoInfoService _fotoInfoService;
        public FotoInfoController(IFotoInfoService fotoInfoService)
        {
            _fotoInfoService = fotoInfoService;
        }

        // GET api/fotoinfo
        public IEnumerable<FotoInfo> Get()
        {
            return _fotoInfoService.GetFotos().ToList(); 
        }

        // GET api/fotoinfo/5
        public FotoInfo Get(int id)
        {
            return _fotoInfoService.GetFotoInfoById(id);
        }

        // POST api/fotoinfo
        public void Post()
        {
            _fotoInfoService.CreateFotoInfo(FotoInfoHelper.GetFotoInfo());
        }

        // PUT api/fotoinfo/5
        public void Put(int id, [FromBody]FotoInfo value)
        {
            _fotoInfoService.UpdateFotoInfo(value);
        }

        // DELETE api/fotoinfo/5
        public void Delete(int id)
        {
            _fotoInfoService.DeleteFotoInfo(id);
        }
    }
}
