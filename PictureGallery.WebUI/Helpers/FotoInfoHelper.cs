﻿using PictureGallery.Model.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PictureGallery.WebUI.Helpers
{
    public static class FotoInfoHelper
    {
        public static FotoInfo GetFotoInfo()
        { 
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files[0];

                if (httpPostedFile != null)
                {
                    byte[] fileData = null;
                    using (var binaryReader = new BinaryReader(httpPostedFile.InputStream))
                    {
                        fileData = binaryReader.ReadBytes(httpPostedFile.ContentLength);
                    }
                    return new FotoInfo() { Foto = fileData};
                }
            }
            return new FotoInfo(); 
        }
    }
}