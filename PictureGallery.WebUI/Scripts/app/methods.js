﻿var methods = methods || {};

methods.uploadFile = function () {
    var bannerImage = $("#bannerImage").val();
    if (bannerImage) {
        var file = document.getElementById('bannerImage').files[0];
        var formData = new FormData();
        formData.append(file.name, file);
        var xhr = new XMLHttpRequest();
        var url = "/api/fotoInfo";
        xhr.open('POST', url, true);
        xhr.onload = function (e) {
            main.showFotoLine();
            var control = $("#bannerImage");
            control.replaceWith(control = control.clone(true));
        };
        xhr.send(formData);  // multipart/form-data
    }
}

methods.getAll = function () {
    return $.ajax({
        url: '/api/fotoinfo',
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        dataType: 'json'
    });
}

methods.getById = function (id) {
    return $.ajax({
        url: '/api/fotoinfo/' + id,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: 'json'
    });
}

methods.updateFile = function (id) {

    var fotoInfo = {
        FotoInfoId: id,
        Foto: "",
        Comment: $("#comment").val()
    };

     $.ajax({
        url: '/api/fotoinfo/' + id,
        type: "Put",
        data: fotoInfo
    });
}

methods.deleteFile = function (id) {
    return $.ajax({
        url: '/api/fotoinfo/' + id,
        type: "Delete",
    });

}
