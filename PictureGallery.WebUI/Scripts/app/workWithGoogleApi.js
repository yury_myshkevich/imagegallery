﻿var googleApi = googleApi || {};

googleApi.getMap = function (data) {
    google.maps.visualRefresh = true;

    var LngValue = data.lng;//53.916667;
    var LatValue = data.lat;//27.55;

    var centr = new google.maps.LatLng(LngValue, LatValue);

    var mapOptions = {
        zoom: 15,
        center: centr
        //mapTypeId: google.maps.MapTypeId.G_NORMAL_MAP
    };

    var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

    var myLatlng = new google.maps.LatLng(LngValue, LatValue);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map
    });
}

googleApi.setCoordinates = function (coordinates) {

    if (coordinates.lng !== "" && coordinates.lat !== "") {
        $("#googleMap")[0].style.display = 'block';
        coordinates.lng = googleApi.toFloat(coordinates.lng.split(" "));
        coordinates.lat = googleApi.toFloat(coordinates.lat.split(" "));
        googleApi.getMap(coordinates);
    }
    else {
        $("#googleMap")[0].style.display = 'none';
    }
}

googleApi.toFloat = function (data) {
    var array = [];
    for (var i = 0; i < data.length; i++) {
        var index = i;
        array[index] = parseFloat(data[index]);
    }
    var param = array[0] + array[1] / 60 + array[2] / 3600;
    return param;
}