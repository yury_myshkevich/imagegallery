﻿var render = render || {};

render.writeFotoLine = function (data) {
    var list = "";

    $.each(data, function (index, info) {
        list += "<li>"
             + "<div class=\"ImgOnImg\"> "
             + "<img id = \"" + info.FotoInfoId + "\" src =\"" + "data:image/jpg;base64," + info.Foto + " \"  class=\"downImg\"  onclick=\"main.showDetails(id)\"  \/> "
             + "<img src=\"/Content/images/close.png\" class=\"upImg\" onclick=\"main.deleteFoto( " + info.FotoInfoId + " )\"/>"
             + "</div></li>";
        //list += "<li><<img id = \"" + info.FotoInfoId + "\" src =\" " + "data:image/jpg;base64," + info.Foto + " \"  width=\"150\" height=\"120\"  onclick=\"main.showDetails(id)\"  \></li>";
    });
    $("#fotoLine").html(list);
}

render.writeBigFoto = function (data) {
    var bigFoto = "";
    bigFoto += "<img src =\" " + "data:image/jpg;base64," + data.Foto + " \" width = \"400\" height = \"243\"    \>";
    $("#bigFoto").html(bigFoto);

}

render.writeExifInfo = function (data) {

    var coordinates = {
        lng: "",
        lat: ""
    };

    
    var exifInfo = "";
    if (!data.ExifInformation)
        exifInfo += "<span class = \"title\">NO EXIF information!</span></br>";
    else {
        exifInfo += "<span class = \"title\">EXIF infonformation : </span></br>";
        $.each(data.ExifInformation, function (key, value) {
            exifInfo += "<span class=\"key\">" + key + "</span> : <span class=\"value\">" + value + "</span><br />"
            if (key === "GPSLatitude")
                coordinates.lng = value;
            if (key === "GPSLongitude")
                coordinates.lat = value;
        });
    }
    $("#exifInfo").html(exifInfo);

    googleApi.setCoordinates(coordinates);
    
}

render.writeComment = function (data) {

    var comment = "";
    comment += "<span>User image discription : </span></br>"
            + "<input id=\"comment\" class=\"comment\" type=\"text\" contenteditable=\"true\" placeholder=\"input comment please\"/>"
            + "<button onclick =\"methods.updateFile(" + data.FotoInfoId + ")\" class=\"button\">Save</button>";
    $("#commentZone").html(comment);
    if (data.Comment !== "")
        $("#comment").val(data.Comment);
}

render.writeCentralArea = function (data) {

    render.writeBigFoto(data);
    render.writeExifInfo(data);
    render.writeComment(data);
}

