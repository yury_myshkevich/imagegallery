﻿var main = main || {};

main.start = function () {
    main.showFotoLine();
}

main.showFotoLine = function () {
    methods.getAll().done(function (data) {
        render.writeFotoLine(data);
    }).fail(function () {
        alert("Failed to get photos");
    });
}

main.showDetails = function (id) {
    methods.getById(id).done(function (data) {
        render.writeCentralArea(data);
    }).fail(function () {
        alert("Failed to get by id");
    });
}

main.deleteFoto = function (id) {
    methods.deleteFile(id).done(function (data) {
        main.showFotoLine();
    }).fail(function () {
        alert("Failed to remove")
    });
}

$("#upload").on("click", function () {
    methods.uploadFile();
});
