﻿using PictureGallery.Model.Models;
using PictureGallery.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureGallery.Service.Interface
{
    public interface IFotoInfoService
    {
        IEnumerable<FotoInfo> GetFotos();
        FotoInfoExtended GetFotoInfoById(int id);
        void CreateFotoInfo(FotoInfo fotoInfo);
        void UpdateFotoInfo(FotoInfo fotoInfo);
        void DeleteFotoInfo(int id);
        void SaveFotoInfo();
    }
}
