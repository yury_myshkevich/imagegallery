﻿using PictureGallery.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PictureGallery.WebUI.Models
{
    public class FotoInfoExtended : FotoInfo
    {
        public Dictionary<string,string> ExifInformation { get; set; }
    }
}