﻿using ImageMagick;
using LevDan.Exif;
using PictureGallery.Data.Interface;
using PictureGallery.Model.Models;
using PictureGallery.Service.Interface;
using PictureGallery.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace PictureGallery.Service.Service
{
    public class FotoInfoService : IFotoInfoService
    {
        private readonly IFotoInfoRepository _fotoInfoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public FotoInfoService(IFotoInfoRepository fotoInfoRepository, IUnitOfWork unitOfWork)
        {
            _fotoInfoRepository = fotoInfoRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<FotoInfo> GetFotos()
        {
            return _fotoInfoRepository.GetAll();
        }

        public FotoInfoExtended GetFotoInfoById(int id)
        {
            var fotoInfo = _fotoInfoRepository.GetById(id);

            Dictionary<string, string> exifInformation = null;
            using (MemoryStream ms = new MemoryStream(fotoInfo.Foto))
            {
                Image returnImage = Image.FromStream(ms);
                ExifTagCollection collection = new ExifTagCollection(returnImage);
                if (collection.Count() > 0)
                {
                    exifInformation = new Dictionary<string, string>();
                    foreach (var item in collection)
                    {
                            exifInformation.Add(item.FieldName.ToString(), item.Value.ToString());
                    }
                }
            }
            return new FotoInfoExtended() { FotoInfoId = fotoInfo.FotoInfoId, Foto = fotoInfo.Foto, Comment = fotoInfo.Comment, ExifInformation = exifInformation };
        }

        public void CreateFotoInfo(FotoInfo fotoInfo)
        {
            _fotoInfoRepository.Add(fotoInfo);
            SaveFotoInfo();
        }

        public void UpdateFotoInfo(FotoInfo fotoInfo)
        {
            fotoInfo.Foto = GetFotoInfoById(fotoInfo.FotoInfoId).Foto;
            _fotoInfoRepository.Update(fotoInfo);
            SaveFotoInfo();
        }

        public void DeleteFotoInfo(int id)
        {
            _fotoInfoRepository.Delete(_fotoInfoRepository.GetById(id));
            SaveFotoInfo();
        }

        public void SaveFotoInfo()
        {
            _unitOfWork.Commit();
        }
    }
}
