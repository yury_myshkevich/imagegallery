﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureGallery.Model.Models
{
    public class FotoInfo
    {
        public int FotoInfoId { get; set; }
        public byte[] Foto { get; set; }
        public string Comment { get; set; }
    }
}
